<!--
SPDX-FileCopyrightText: 2023 Thomas Kramer

SPDX-License-Identifier: CC-BY-SA-4.0
-->

# rwISOmount

* Mount optical discs or ISO images as a read-write overlay filesystem.
* Do changes to the mounted overlay FS.
* Write the changes to the ISO image or optical disc.

## Goals

* Easily modify content on optical discs
* Use optical discs as storage backends for `borg` backup

## Dependencies

* xorriso (`apt install xorriso`)

## Example

```sh
# Create an ISO image (or use a CD/DVD/Bluray drive like /dev/sr0 with a writable optical disc inside)
truncate -s 1G /tmp/MyIsoImage.iso
xorriso -dev /tmp/MyIsoImage.iso -mkdir myDir

# Mount the ISO image and run bash in the mounted overlay directory
rwisomount --cd /tmp/MyIsoImage.iso:myDir bash

# Now `bash` is running with the working directory pointing to the new overlay FS.
# Do some modifications here:
echo "Hello World!" >> hello.txt

# Exit bash.
exit

# Inspect the ISO image
xorriso -dev /tmp/MyIsoImage.iso -lsl
```

## Example: backups to Bluray with borg
[Borg backup](https://www.borgbackup.org/) is a very efficient backup tool. Unfortunately it lacks
the option to store backups on optical discs. (Bluray mdisc is quite an interesting backup medium due to longevity and low costs)

`rwisomount` helps to create backups with borg to optical discs.


```
DRIVE=/dev/sr0
TMP_DIR=/tmp/rwisomount # This is best to remain constant for borg.

RWMOUNT="./rwisomount --cd --close --tmp-dir $TMP_DIR $DRIVE"

# Initialize the drive
xorriso -dev $DRIVE -mkdir backup

# Initalize the backup directory.
$RWMOUNT borg init --encryption=none --append-only backup/

# Create a backup
$RWMOUNT borg create --stats backup::myArchive1 $HOME

# List the archives
$RWMOUNT borg list backup/

```

## Licence: GPL-3.0-or-later
